package com.jmrodrigg.printing.model;

/**
 * Created by marcos on 08/10/2016.
 */
public class Friend {
    private String name;
    private String id;

    public Friend(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
