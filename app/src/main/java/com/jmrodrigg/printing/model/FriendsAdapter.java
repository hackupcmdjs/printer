package com.jmrodrigg.printing.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jmrodrigg.printing.PdfGetter;
import com.jmrodrigg.printing.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcos on 08/10/2016.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.AdapterViewHolder> {
    List<Friend> mFriendList;

    public FriendsAdapter() {
        mFriendList = new ArrayList<>();
    }

    public FriendsAdapter(List<Friend> l) {
        mFriendList = l;
        notifyDataSetChanged();
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Instancia un layout XML en la correspondiente vista.
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        //Inflamos en la vista el layout para cada elemento
        View view = inflater.inflate(R.layout.rowlayout, parent, false);
        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterViewHolder holder, final int position) {
        holder.name.setText(mFriendList.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(holder.context, PdfGetter.class);
                i.putExtra("id",mFriendList.get(position).getId());
                holder.context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFriendList.size();
    }

    public void setData(List<Friend> l) {
        mFriendList.clear();
        mFriendList.addAll(l);
        notifyDataSetChanged();
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public Context context;
        public AdapterViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            this.name = (TextView) itemView.findViewById(R.id.name);
        }
    }
}
