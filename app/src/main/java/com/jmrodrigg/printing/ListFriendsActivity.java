package com.jmrodrigg.printing;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.facebook.AccessToken;
import com.jmrodrigg.printing.model.Constants;
import com.jmrodrigg.printing.model.Friend;
import com.jmrodrigg.printing.model.FriendsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ListFriendsActivity extends Activity {
    LinearLayoutManager mLinearLayout;
    RecyclerView mRecyclerView;
    FriendsAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_friends);

        MyAsyncTask m = new MyAsyncTask();
        m.execute();

        //findViewById del layout activity_main
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        //LinearLayoutManager necesita el contexto de la Activity.
        //El LayoutManager se encarga de posicionar los items dentro del recyclerview
        //Y de definir la politica de reciclaje de los items no visibles.
        mLinearLayout = new LinearLayoutManager(this);

        //Asignamos el LinearLayoutManager al recycler:
        mRecyclerView.setLayoutManager(mLinearLayout);

        //El adapter se encarga de  adaptar un objeto definido en el código a una vista en xml
        //según la estructura definida.
        //Asignamos nuestro custom Adapter
        mAdapter = new FriendsAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    public class MyAsyncTask extends AsyncTask<String, Void, List<Friend>>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Friend> doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            String url = Constants.API_URL + "friendslist?access_token="+ AccessToken.getCurrentAccessToken().getToken();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();

                List<Friend> l = null;
                try {
                    JSONArray arr = new JSONArray(response.body().string());
                    l = new ArrayList<>();
                    for(int i = 0; i < arr.length(); ++i){
                        JSONObject friend = arr.getJSONObject(i);
                        if(!friend.isNull("id")) {
                            Friend f = new Friend(friend.getString("id"), friend.getString("name"));
                            l.add(f);
                        }
                    }
                    return l;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.v("ERROR", e.getMessage());
                e.printStackTrace();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<Friend> l) {
            mRecyclerView.setAdapter(new FriendsAdapter(l));
        }


    }
}
