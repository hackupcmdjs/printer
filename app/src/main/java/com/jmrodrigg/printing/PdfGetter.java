package com.jmrodrigg.printing;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.jmrodrigg.printing.model.Constants;
import com.jmrodrigg.printing.model.FileDownloader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PdfGetter extends Activity {
    String userId;
    final String generatePdfRequest = Constants.API_URL + "pdf?userId=" + userId + "access_token=" + AccessToken.getCurrentAccessToken().getToken();

    String retrievePdfRequest = Constants.API_URL + "collage/";
    MessageReceiver m;
    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_getter);
        userId = getIntent().getExtras().getString("id");
        IntentFilter pdfGeneratedFilter = new IntentFilter("pdfGenerated");
        m = new MessageReceiver();
        this.registerReceiver(m, pdfGeneratedFilter);

        try {
            socket = IO.socket(Constants.API_URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();
        socket.emit("test");
        socket.on("pdfGenerated", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.v("SOCKET", "PDF GENERATED");
                JSONObject obj = (JSONObject)args[0];
                String uid = "";
                try {
                    uid = obj.getString("uid");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent();
                intent.setAction("pdfGenerated");
                intent.putExtra("uid", uid);
                getApplicationContext().sendBroadcast(intent);
            }
        });

        String TAG = "PERMISSIONS";
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
        }


        //TODO hacer una request A /pdf
        new requestPDFThread().execute(userId);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(m);
    }



    private class requestPDFThread extends AsyncTask<String, String, Void> {
        protected Void doInBackground(String... userId) {
            //TODO A PDF
            Log.v("THREAD",userId[0]);

            OkHttpClient client = new OkHttpClient();
            String url = Constants.API_URL + "pdf?userId=" + userId[0] + "&access_token=" + AccessToken.getCurrentAccessToken().getToken();

            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                if(response.isSuccessful()) Log.v("RESPONSE", "IS OK");
                else Log.v("RESPONSE", "NOT OK " );
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Long result) {
            // This is executed in main Thread, use the result
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("Permissions","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }

    private class retrievePDFThread extends AsyncTask<String, String, Void> {
        protected Void doInBackground(String... uid) {
            Log.v("WOW","RETRIEVE PDF STARTED");

            String fileUrl = Constants.API_URL + "collage/" + uid[0];
            String fileName = uid[0] + ".pdf";


            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory + "/collage");
            Log.v("FOLDER",folder.getAbsolutePath());

            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            Log.v("PDFFILE",pdfFile.getPath());
            FileDownloader.downloadFile(fileUrl, pdfFile);
            Uri path = Uri.fromFile(pdfFile);
            Log.v("uri",path.toString());

            return null;
        }


        protected void onPostExecute(Long result) {
            // This is executed in main Thread, use the result
        }
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String uid = (String) intent.getExtras().get("uid");
            Log.v("RECEIVER ", "Logged uid " + uid);
            new retrievePDFThread().execute(uid);
        }
    }
}
